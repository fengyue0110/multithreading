#include "thread_node.h"

namespace threading{

    void hello_world(int x)
    {
        std::cout<<"hello world I am is :"<<x<<endl;   
    }

}

int main()
{
    thread thread1(threading::hello_world,1);
    thread thread2(threading::hello_world,2);
    thread1.join();
    thread2.join();
    return 0;
}